<?php

return [
    'url' => env('EMAIL_VERIFY', 'https://api.emailverifyapi.com/v3/lookups/json'),
    'key' => env('EMAIL_VERIFY_KEY','A524EF919BE22046'),
    'active_real_mail' => env('ACTIVE_REAL_MAIL', true),
];

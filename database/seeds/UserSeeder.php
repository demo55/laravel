<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::query()->truncate();
        $users = [
            [
                'email' => 'nhund@funtap.vn',
                'name' => 'Nguyễn Đình Nhu',
                'password' => bcrypt(12345678),
                'status' => User::ACTIVE,
            ] ,
            [
                'email' => 'longnt@funtap.vn',
                'name' => 'Long nguyen',
                'password' => bcrypt(12345678),
                'status' => User::INACTIVE,
            ]
        ];

        User::insert($users);
    }
}

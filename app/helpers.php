<?php

use Zttp\Zttp;

if (!function_exists('validate_real_email')) {
    /**
     * Check if a given email is real or not.
     *
     * @param  mixed $value
     * @param  string $locale
     * @return string
     * @throws \Exception
     */
    function validate_real_email($value)
    {
        if (! config('email.active_real_mail')) {
            return true;
        }
        try {
            $uri = config('email.url') ;
            $data = Zttp::get($uri, [
                'key' => config('email.key'),
                'email' => $value,
            ])->json();
            $result =json_decode(json_encode($data));
            return $result->deliverable;
        } catch (Exception $e) {
            return false;
        }

    }
}

<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class ValidatorProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->validateRealMail();
    }

    /**
     * check the mail is active or Inactive
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function validateRealMail(){
        $this->app->make('validator')->extend('real_email', function ($attribute, $value, $parameters, $validator) {
//            return validate_real_email($value);
            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                return false;
            }
            return  true;
        });
    }
}

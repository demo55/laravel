<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UseLoginRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'grant_type' => 'string',
            'client_id' => 'required|string|exists:oauth_clients,id',
            'client_secret' => 'required|string',
            'password' => 'required|string',
            'email' =>'required|real_email|exists:users,email',
        ];
    }
    public function messages()
    {

        return [
            'email.real_email' => 'the mail is not active'
        ];
    }
}

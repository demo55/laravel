<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\UseLoginRequest;
use App\Models\Transformers\UserTransformer;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @param UseLoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function login(UseLoginRequest $request)
    {
        $tokenEntity = app('authenticator')->getTokensGrantPass(
            $request->input('client_id'),
            $request->input('client_secret'),
            $request->input('email'),
            $request->input('password')
        );

        return response()->json($tokenEntity);
    }

    public function show(Request $request)
    {
        return fractal()->item($request->user(), new UserTransformer);
    }

    public function update(Request $request)
    {
        $user = $request->user();

        $user->updated_at = now();
        $user->save();

        return fractal()->item($request->user(), new UserTransformer);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout(Request $request)
    {
        $request->user()->token()->revoke();

        return response()->json(['message' => 'logout was successful']);
    }
}

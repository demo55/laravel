<?php

namespace App\Http\Middleware;

use App\Models\User;
use Closure;

class ActiveUserMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();
        if (!$user || $user->status == User::INACTIVE){
            abort(403, 'User account is inactive');
        }

        return $next($request);
    }
}

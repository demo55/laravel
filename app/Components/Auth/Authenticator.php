<?php

namespace App\Components\Auth;

use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Response;
use Illuminate\Support\Arr;
use Laravel\Passport\Client;
use Laravel\Passport\Passport;

class Authenticator
{
    /**
     * get token user
     *
     * @param $grantType
     * @param $clientId
     * @param $clientSecret
     * @param $username
     * @param $password
     * @param string $scope
     * @return Response
     * @throws AuthorizationException
     */
    public function getTokensGrantPass($clientId, $clientSecret, $username, $password, $grantType = 'password' ,  $scope = ['user:read'])
    {
        $client = $this->retrieveClient($clientId);

        if ($client->secret !== $clientSecret) {
            throw new AuthorizationException('The provided client credentials are invalid.');
        }

        $request = request()->create('/oauth/token', 'POST', [
            'grant_type' => $grantType,
            'client_id'  => $client->id,
            'client_secret' => $client->secret,
            'username' => $username,
            'password' => $password,
            'scope' => $scope,
        ]);

        return $this->handleTokenRequest($request);
    }


    /**
     * @param $request
     * @return Response|mixed
     */
    protected function handleTokenRequest($request)
    {
        $response = app()->handle($request);

        if (!($response instanceof Response && $response->status() >= 200 && $response->status() < 300)) {
            return $response;
        }

        $tokens = json_decode((string) $response->getContent(), true);

        if (Arr::has($tokens, 'errors') || !Arr::has($tokens, 'access_token')) {
            return $response;
        }

        return $tokens;
    }



    /**
     * @param $clientId
     * @return mixed
     * @throws AuthorizationException
     */
    protected function retrieveClient($clientId)
    {
        if (empty($clientId) || !is_string($clientId)) {
            throw new AuthorizationException('The provided client ID is invalid.');
        }

        $client = Client::where('id', $clientId)->where('revoked', false)->first();

        $this->validateClient($client);

        return $client;
    }

    /**
     * @param $client
     * @throws AuthorizationException
     */
    protected function validateClient($client)
    {
        if (empty($client) || !$client instanceof Client) {
            throw new AuthorizationException('The provided client is invalid or revoked.');
        }
    }
}

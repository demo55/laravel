<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function (){
    Route::get('/me', 'Auth\UserController@show')->middleware('scopes:user:read');
    Route::post('/logout', 'Auth\UserController@logout');
});

Route::middleware(['auth:api', 'active-user'])->group(function (){
    Route::post('/me', 'Auth\UserController@update');
});

Route::middleware([])->group(function (){
    Route::post('login', 'Auth\UserController@login');
});
